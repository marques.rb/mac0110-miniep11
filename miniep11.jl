#MAC0110 - MiniEP11
# Beatriz Marques - 11932334

using Unicode
function trataPalindromo(string)
    string = Unicode.normalize(string,stripmark=true,casefold=true)
    lista = []
    for el in 1:length(string)
    	if string[el] >= 'a' && string[el] <= 'z'
    	append!(lista,string[el])
   	 end
    end
    return lista = join(lista)
end

function palindromo(string)
   string = trataPalindromo(string)
   tam = length(string)
   if tam == 0
    return true
   end
   k = 0
   for esquerda in 1:length(string)
      for direita in string[end-k]
         if string[esquerda] == direita
            k += 1
            break
         else
            return false
         end
      end
   end
return true
end

#Testes
using Test
function testePalindromo()
   @test palindromo("") == true
   @test palindromo("Ana") == true 
   @test palindromo("Mentira") == false
   @test palindromo("Verdade") == false 
   @test palindromo("Socorram-me, subi no ônibus em Marrocos!") == true
   @test palindromo("Ame o poema.") == true
   println("Final dos testes")
end
testePalindromo()



